# sert_exam

## Jenkins pipeline:
```
- pipeline {
  agent any

  stages {
    stage('Provision Infrastructure') {
      steps {
        sh '''
          cd terraform/
          terraform init
          terraform apply -auto-approve
        '''
      }
    }

    stage('Deploy Application') {
      steps {
        ansiblePlaybook credentialsId: 'ansible-ssh', inventory: 'inventory/stage', playbook: 'deploy.yml'
      }
    }
  }

  post {
    always {
      sh '''
        cd terraform/
        terraform destroy -auto-approve
      '''
    }
  }
}
-
```

## Terraform code 
```
- provider "aws" {
  region = "us-west-2"
}

resource "aws_instance" "ec2" {
  ami           = "ami-0c55b159cbfafe1f0"
  instance_type = "t2.micro"

  tags = {
    Name = "EC2 Instance"
  }
}
resource "aws_security_group" "instance_sg" {
  name_prefix = "instance_sg_"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```
- 
## Ansible playbook:
```
- hosts: stage
  become: yes

  tasks:
  - name: Install Docker
    apt:
      name: docker.io
      state: present

  - name: Build Application Image
    docker_image:
      build:
        context: "{{ playbook_dir }}/app"
      name: my-app
      tag: latest

  - name: Run Container
    docker_container:
      name: my-app
      image: my-app:latest
      ports:
        - "8080:8080"n
```